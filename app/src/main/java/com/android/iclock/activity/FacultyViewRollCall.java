package com.android.iclock.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.iclock.bean.MarkAttendance;
import com.android.iclock.bean.Student;
import com.android.iclock.context.ContextiClock;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

public class FacultyViewRollCall extends Activity {


    MyDatabase adapter_database = new MyDatabase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.markrollcall);

        list_View = (ListView) findViewById(R.id.listview);
        final ArrayList<String> attendance_Array_List = new ArrayList<String>();
        attendance_Array_List.add("Id | Student Name |  Status");

        attendance_Bean_ArrayList = ((ContextiClock) FacultyViewRollCall.this.getApplicationContext()).getRollCall();

        for (MarkAttendance attendance_Bean : attendance_Bean_ArrayList) {
            String role = "";
            if (attendance_Bean.retrieveRollCallStatus() != 0) {
                MyDatabase adapter_database = new MyDatabase(FacultyViewRollCall.this);
                Student studentBean = adapter_database.retrieveStudent(attendance_Bean.retrieveStudentRollCall());
                role = attendance_Bean.retrieveStudentRollCall() + ".     " + studentBean.retrieveFirstName() + "," + studentBean.retrieveLastName() + "                  " + attendance_Bean.retrieveRollCall();
            } else {
                role = attendance_Bean.retrieveRollCall();
            }

            attendance_Array_List.add(role);
            Log.d("users: ", role);

        }

        list_Array_Adapter = new ArrayAdapter<String>(this, R.layout.view_rollcall_list, R.id.labelAttendance, attendance_Array_List);
        list_View.setAdapter(list_Array_Adapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }


    private ArrayAdapter<String> list_Array_Adapter;
    ArrayList<MarkAttendance> attendance_Bean_ArrayList;
    private ListView list_View;

}
