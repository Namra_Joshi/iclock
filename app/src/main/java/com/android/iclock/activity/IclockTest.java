package com.android.iclock.activity;

import java.util.ArrayList;

import com.android.iclock.bean.RollCallStatus;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class IclockTest extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iclocktest);

        button_submit = (Button) findViewById(R.id.button1);
        button_submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View argument0) {
                MyDatabase adapter_database = new MyDatabase(IclockTest.this);
                RollCallStatus attendance_Session_Bean = new RollCallStatus();


                attendance_Session_Bean.setInstructorStatus(1);
                attendance_Session_Bean.setMajor("CS");
                attendance_Session_Bean.setClass("Android");
                attendance_Session_Bean.setDate("11/06/2021");
                attendance_Session_Bean.setCourse("MyDataBase");

                adapter_database.markPresence(attendance_Session_Bean);
                Log.d("add", "inserted");

                ArrayList<RollCallStatus> attendance_Session_Bean_ArrayList = adapter_database.retrieveTotalPresentStudent();

                for (RollCallStatus session_Bean : attendance_Session_Bean_ArrayList) {
                    Log.d("for", "in for loop");
                    int newAID = session_Bean.retrieveRollCallStatus();
                    int newFID = session_Bean.retrieveInstructorStatus();
                    String newClass = session_Bean.retrieveClass();
                    String major = session_Bean.retrieveMajor();
                    String new_string_date = session_Bean.retrieveDate();
                    String new_string_course = session_Bean.retrieveCourse();
                    Log.d("id", newAID + "");
                    Log.d("fid", newFID + "");
                    Log.d("sclass", newClass);
                    Log.d("dept", major);
                    Log.d("date", new_string_date);
                    Log.d("sub", new_string_course);
                }
            }
        });
    }

    Button button_submit;
}
