package com.android.iclock.activity;

import com.android.iclock.bean.Faculty;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertFaculty extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insertfaculty);


        first_name_text = (EditText) findViewById(R.id.editTextFirstName);
        last_name_text = (EditText) findViewById(R.id.editTextLastName);
        mobile_edit_text = (EditText) findViewById(R.id.editTextPhone);
        address_edit_text = (EditText) findViewById(R.id.editTextaddr);
        username_edit_text = (EditText) findViewById(R.id.editTextUserName);
        password_edit_text = (EditText) findViewById(R.id.editTextPassword);
        button_register = (Button) findViewById(R.id.RegisterButton);

        button_register.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String firstNameString = first_name_text.getText().toString();
                String lastNameString = last_name_text.getText().toString();
                String mobileNumber = mobile_edit_text.getText().toString();
                String addressString = address_edit_text.getText().toString();
                String userNameString = username_edit_text.getText().toString();
                String passWordString = password_edit_text.getText().toString();

                if (TextUtils.isEmpty(firstNameString)) {
                    first_name_text.setError("Please enter your Firstname!!!");
                } else if (TextUtils.isEmpty(lastNameString)) {
                    last_name_text.setError("Please enter your Lastname!!!");
                } else if (TextUtils.isEmpty(mobileNumber)) {
                    mobile_edit_text.setError("Please enter your Mobile Number!!!");
                } else if (TextUtils.isEmpty(addressString)) {
                    address_edit_text.setError("Please enter your Address!");
                } else if (TextUtils.isEmpty(userNameString)) {
                    mobile_edit_text.setError("Please enter your Username!!!");
                } else if (TextUtils.isEmpty(passWordString)) {
                    address_edit_text.setError("Please enter your Password!!!");
                } else {

                    Faculty new_faculty_Bean = new Faculty();
                    new_faculty_Bean.setFirstName(firstNameString);
                    new_faculty_Bean.setLastName(lastNameString);
                    new_faculty_Bean.setNumber(mobileNumber);
                    new_faculty_Bean.setAddress(addressString);
                    new_faculty_Bean.setUserName(userNameString);
                    new_faculty_Bean.setPassword(passWordString);

                    MyDatabase adapter_database = new MyDatabase(InsertFaculty.this);
                    adapter_database.insertInstructor(new_faculty_Bean);

                    Intent new_intent = new Intent(InsertFaculty.this, MenuScreen.class);
                    startActivity(new_intent);
                    Toast.makeText(getApplicationContext(), "Faculty added successfully!!!", Toast.LENGTH_SHORT).show();

                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }


    EditText first_name_text, last_name_text, mobile_edit_text, address_edit_text, username_edit_text, password_edit_text;
    Button button_register;
}
