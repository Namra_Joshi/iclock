package com.android.iclock.activity;

import com.android.iclock.bean.Student;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class InsertStudent extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insertstudent);

        major_sp = (Spinner) findViewById(R.id.spinnerdept);
        college_year_sp = (Spinner) findViewById(R.id.spinneryear);
        first_name_text = (EditText) findViewById(R.id.editTextFirstName);
        last_name_text = (EditText) findViewById(R.id.editTextLastName);
        mobile_edit_text = (EditText) findViewById(R.id.editTextPhone);
        address_edit_text = (EditText) findViewById(R.id.editTextaddr);
        button_register = (Button) findViewById(R.id.RegisterButton);

        major_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                ((TextView) arg0.getChildAt(0)).setTextColor(Color.BLACK);
                major = (String) major_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> major_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, major_string_array);
        major_adapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        major_sp.setAdapter(major_adapter);


        college_year_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                ((TextView) arg0.getChildAt(0)).setTextColor(Color.BLACK);
                college_year = (String) college_year_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> college_year_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, college_year_array);
        college_year_adapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        college_year_sp.setAdapter(college_year_adapter);


        button_register.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                String firstNameString = first_name_text.getText().toString();
                String lastNameString = last_name_text.getText().toString();
                String mobileNumber = mobile_edit_text.getText().toString();
                String addressString = address_edit_text.getText().toString();

                if (TextUtils.isEmpty(firstNameString)) {
                    first_name_text.setError("Please enter your Firstname!!!");
                } else if (TextUtils.isEmpty(lastNameString)) {
                    last_name_text.setError("Please enter your Lastname!!!");
                } else if (TextUtils.isEmpty(mobileNumber)) {
                    mobile_edit_text.setError("Please enter your Mobile Number!!!");
                } else if (TextUtils.isEmpty(addressString)) {
                    address_edit_text.setError("Please enter your Address!");
                } else {

                    Student new_faculty_Bean = new Student();

                    new_faculty_Bean.setFirstName(firstNameString);
                    new_faculty_Bean.setLastName(lastNameString);
                    new_faculty_Bean.setNumber(mobileNumber);
                    new_faculty_Bean.setAddress(addressString);
                    new_faculty_Bean.setMajor(major);
                    new_faculty_Bean.setCourse(college_year);

                    MyDatabase adapter_database = new MyDatabase(InsertStudent.this);
                    adapter_database.insertStudent(new_faculty_Bean);

                    Intent new_intent = new Intent(InsertStudent.this, MenuScreen.class);
                    startActivity(new_intent);
                    Toast.makeText(getApplicationContext(), "Student added successfully!!!", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    private String[] major_string_array = new String[]{"CS"};
    private String[] college_year_array = new String[]{"Freshman", "Sophomore", "Junior", "Senior"};
    Button button_register;
    EditText first_name_text, last_name_text, mobile_edit_text, address_edit_text;
    Spinner major_sp, college_year_sp;
    String major, college_year;

}
