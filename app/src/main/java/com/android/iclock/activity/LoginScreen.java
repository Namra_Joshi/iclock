package com.android.iclock.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.iclock.bean.Faculty;
import com.android.iclock.context.ContextiClock;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

public class LoginScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginscreen);

        button_login = (Button) findViewById(R.id.buttonlogin);
        username_edit_text = (EditText) findViewById(R.id.editTextusername);
        password_edit_text = (EditText) findViewById(R.id.editTextpassword);
        login_person_sp = (Spinner) findViewById(R.id.spinnerloginas);

        login_person_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.WHITE);
                domain_or_faculty = (String) login_person_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> roleType_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, typeOfUserString);
        roleType_adapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        login_person_sp.setAdapter(roleType_adapter);

        button_login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (domain_or_faculty.equals("Domain")) {

                    String userNameString = username_edit_text.getText().toString();
                    String passWordString = password_edit_text.getText().toString();

                    if (TextUtils.isEmpty(userNameString)) {
                        username_edit_text.setError("Invalid User Name :(");
                    } else if (TextUtils.isEmpty(passWordString)) {
                        password_edit_text.setError("Enter Password!!!");
                    } else {
                        if (userNameString.equals("Domain") & passWordString.equals("domain123")) {
                            Intent new_intent = new Intent(LoginScreen.this, MenuScreen.class);
                            startActivity(new_intent);
                            Toast.makeText(getApplicationContext(), "Login successful!!!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Login failed :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    String userNameString = username_edit_text.getText().toString();
                    String passWordString = password_edit_text.getText().toString();

                    if (TextUtils.isEmpty(userNameString)) {
                        username_edit_text.setError("Invalid User Name :(");
                    } else if (TextUtils.isEmpty(passWordString)) {
                        password_edit_text.setError("Enter Password!!!");
                    }
                    MyDatabase adapter_database = new MyDatabase(LoginScreen.this);
                    Faculty new_faculty_Bean = adapter_database.instructorValidation(userNameString, passWordString);

                    if (new_faculty_Bean != null) {
                        Intent new_intent = new Intent(LoginScreen.this, RollCall.class);
                        startActivity(new_intent);
                        ((ContextiClock) LoginScreen.this.getApplicationContext()).setInstructor(new_faculty_Bean);
                        Toast.makeText(getApplicationContext(), "Login successful!!!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Login failed :(", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    Spinner login_person_sp;
    String domain_or_faculty;
    private String[] typeOfUserString = new String[]{"Domain", "Faculty Member"};
    Button button_login;
    EditText username_edit_text, password_edit_text;

}
