package com.android.iclock.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.android.iclock.bean.MarkAttendance;
import com.android.iclock.bean.Student;
import com.android.iclock.context.ContextiClock;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

public class MarkRollCall extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.markrollcall);

        id_session = getIntent().getExtras().getInt("sessionId");


        list_view = (ListView) findViewById(R.id.listview);
        final ArrayList<String> studentList = new ArrayList<String>();

        student_Bean_ArrayList = ((ContextiClock) MarkRollCall.this.getApplicationContext()).getStudent();


        for (Student student_Bean : student_Bean_ArrayList) {
            String string_user = student_Bean.retrieveFirstName() + "," + student_Bean.retrieveLastName();

            studentList.add(string_user);
            Log.d("users: ", string_user);

        }

        list_adapter = new ArrayAdapter<String>(this, R.layout.insertattendance, R.id.labelA, studentList);
        list_view.setAdapter(list_adapter);

        list_view.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> argument0, View argument1, int argument2,
                                    long argument3) {

                argument0.getChildAt(argument2).setBackgroundColor(Color.TRANSPARENT);
                argument1.setBackgroundColor(334455);
                final Student student_Bean = student_Bean_ArrayList.get(argument2);
                final Dialog new_dialog = new Dialog(MarkRollCall.this);
                new_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                new_dialog.setContentView(R.layout.iclock_layout_test);
                RadioGroup new_radio_Group;
                RadioButton radio_button_present;
                RadioButton radio_button_absent;
                new_radio_Group = (RadioGroup) new_dialog.findViewById(R.id.radioGroup);
                radio_button_present = (RadioButton) new_dialog.findViewById(R.id.PresentradioButton);
                radio_button_absent = (RadioButton) new_dialog.findViewById(R.id.AbsentradioButton);
                new_radio_Group.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == R.id.PresentradioButton) {

                            test_status = "P";
                        } else if (checkedId == R.id.AbsentradioButton) {

                            test_status = "A";
                        } else {
                        }
                    }
                });

                submit_button_attendance = (Button) new_dialog.findViewById(R.id.attendanceSubmitButton);
                submit_button_attendance.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View argument0) {
                        MarkAttendance attendance_Bean = new MarkAttendance();

                        attendance_Bean.setRollCallStatus(id_session);
                        attendance_Bean.setStudentPresence(student_Bean.retrieveStudent());
                        attendance_Bean.setRollCall(test_status);

                        MyDatabase adapter_database = new MyDatabase(MarkRollCall.this);
                        adapter_database.insertRollCall(attendance_Bean);

                        new_dialog.dismiss();

                    }
                });

                new_dialog.setCancelable(true);
                new_dialog.show();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    int id_session = 0;
    String test_status = "P";
    Button submit_button_attendance;
    ArrayList<Student> student_Bean_ArrayList;
    private ListView list_view;
    private ArrayAdapter<String> list_adapter;

}
