package com.android.iclock.activity;

import java.util.ArrayList;

import com.android.iclock.bean.MarkAttendance;
import com.android.iclock.context.ContextiClock;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuScreen extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuscreen);

        button_add_Student = (Button) findViewById(R.id.buttonaddstudent);
        button_add_Faculty = (Button) findViewById(R.id.buttonaddfaculty);
        button_view_Student = (Button) findViewById(R.id.buttonViewstudent);
        button_view_Faculty = (Button) findViewById(R.id.buttonviewfaculty);
        button_logout = (Button) findViewById(R.id.buttonlogout);

        button_add_Student.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent new_intent = new Intent(MenuScreen.this, InsertStudent.class);
                startActivity(new_intent);
            }
        });

        button_add_Faculty.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent new_intent = new Intent(MenuScreen.this, InsertFaculty.class);
                startActivity(new_intent);
            }
        });

        button_view_Faculty.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent new_intent = new Intent(MenuScreen.this, FacultyView.class);
                startActivity(new_intent);
            }
        });


        button_view_Student.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent new_intent = new Intent(MenuScreen.this, StudentView.class);
                startActivity(new_intent);
            }
        });
        button_logout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent new_intent = new Intent(MenuScreen.this, MainActivity.class);
                new_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(new_intent);
            }
        });
        button_attendance_Per_Student = (Button) findViewById(R.id.attendancePerStudentButton);
        button_attendance_Per_Student.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View argument0) {

                MyDatabase adapter_database = new MyDatabase(MenuScreen.this);
                ArrayList<MarkAttendance> attendance_Bean_ArrayList = adapter_database.getAllAttendanceByStudent();
                ((ContextiClock) MenuScreen.this.getApplicationContext()).setAttendanceBeanList(attendance_Bean_ArrayList);

                Intent new_intent = new Intent(MenuScreen.this, RollCallPerStudent.class);
                startActivity(new_intent);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    Button button_add_Student, button_add_Faculty, button_view_Student, button_view_Faculty, button_logout, button_attendance_Per_Student;
}
