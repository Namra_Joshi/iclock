package com.android.iclock.activity;

import java.util.ArrayList;
import java.util.Calendar;

import com.android.iclock.bean.MarkAttendance;
import com.android.iclock.bean.RollCallStatus;
import com.android.iclock.bean.Faculty;
import com.android.iclock.bean.Student;
import com.android.iclock.context.ContextiClock;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RollCall extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rollcall);


        branch_sp = (Spinner) findViewById(R.id.spinner1);
        year_sp = (Spinner) findViewById(R.id.spinneryear);
        subject_sp = (Spinner) findViewById(R.id.spinnerSE);

        ArrayAdapter<String> major_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, major_string_array);
        major_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        branch_sp.setAdapter(major_adapter);
        branch_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.BLACK);
                major = (String) branch_sp.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> argument0) {
                // TODO Auto-generated method stub
            }
        });


        ArrayAdapter<String> college_year_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, college_year_array);
        college_year_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year_sp.setAdapter(college_year_adapter);
        year_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.BLACK);
                college_year = (String) year_sp.getSelectedItem();
                Toast.makeText(getApplicationContext(), "year:" + college_year, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onNothingSelected(AdapterView<?> argument0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> course_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tier_final_array);
        course_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subject_sp.setAdapter(course_adapter);
        subject_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.BLACK);
                course = (String) subject_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> argument0) {
                // TODO Auto-generated method stub
            }
        });


        button_date = (ImageButton) findViewById(R.id.DateImageButton);
        my_calendar = Calendar.getInstance();
        var_day = my_calendar.get(Calendar.DAY_OF_MONTH);
        var_month = my_calendar.get(Calendar.MONTH);
        var_year = my_calendar.get(Calendar.YEAR);
        editText_date = (EditText) findViewById(R.id.DateEditText);
        button_date.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View argument0) {
                showDialog(0);

            }
        });

        button_submit = (Button) findViewById(R.id.buttonsubmit);
        button_submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View argument0) {

                RollCallStatus attendance_Session_Bean = new RollCallStatus();
                Faculty bean = ((ContextiClock) RollCall.this.getApplicationContext()).getInstructor();

                attendance_Session_Bean.setInstructorStatus(bean.retrieveInstructor());
                attendance_Session_Bean.setMajor(major);
                attendance_Session_Bean.setClass(college_year);
                attendance_Session_Bean.setDate(editText_date.getText().toString());
                attendance_Session_Bean.setCourse(course);

                MyDatabase adapter_database = new MyDatabase(RollCall.this);
                int id_session = adapter_database.markPresence(attendance_Session_Bean);

                ArrayList<Student> student_Bean_ArrayList = adapter_database.retrieveStudentsByMajor(major, college_year);
                ((ContextiClock) RollCall.this.getApplicationContext()).setStudent(student_Bean_ArrayList);


                Intent new_intent = new Intent(RollCall.this, MarkRollCall.class);
                new_intent.putExtra("sessionId", id_session);
                startActivity(new_intent);
            }
        });

        button_view_Attendance = (Button) findViewById(R.id.viewAttendancebutton);
        button_view_Attendance.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View argument0) {

                RollCallStatus attendance_Session_Bean = new RollCallStatus();
                Faculty new_Faculty_bean = ((ContextiClock) RollCall.this.getApplicationContext()).getInstructor();

                attendance_Session_Bean.setInstructorStatus(new_Faculty_bean.retrieveInstructor());
                attendance_Session_Bean.setMajor(major);
                attendance_Session_Bean.setClass(college_year);
                attendance_Session_Bean.setDate(editText_date.getText().toString());
                attendance_Session_Bean.setCourse(course);

                MyDatabase adapter_database = new MyDatabase(RollCall.this);

                ArrayList<MarkAttendance> attendance_Bean_ArrayList = adapter_database.retrieveRollCall(attendance_Session_Bean);
                ((ContextiClock) RollCall.this.getApplicationContext()).setAttendanceBeanList(attendance_Bean_ArrayList);

                Intent new_intent = new Intent(RollCall.this, FacultyViewRollCall.class);
                startActivity(new_intent);

            }
        });

        button_view_Total_Attendance = (Button) findViewById(R.id.viewTotalAttendanceButton);
        button_view_Total_Attendance.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View argument0) {
                RollCallStatus attendance_Session_Bean = new RollCallStatus();
                Faculty new_Faculty_bean = ((ContextiClock) RollCall.this.getApplicationContext()).getInstructor();

                attendance_Session_Bean.setInstructorStatus(new_Faculty_bean.retrieveInstructor());
                attendance_Session_Bean.setMajor(major);
                attendance_Session_Bean.setClass(college_year);
                attendance_Session_Bean.setCourse(course);

                MyDatabase adapter_database = new MyDatabase(RollCall.this);

                ArrayList<MarkAttendance> attendance_Bean_ArrayList = adapter_database.retrieveTotalPresentStudent(attendance_Session_Bean);
                ((ContextiClock) RollCall.this.getApplicationContext()).setAttendanceBeanList(attendance_Bean_ArrayList);

                Intent new_intent = new Intent(RollCall.this, FacultyViewRollCall.class);
                startActivity(new_intent);

            }
        });
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, var_year, var_month, var_day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            editText_date.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                    + selectedYear);
        }
    };



    private String[] major_string_array = new String[]{"CS"};
    private String[] college_year_array = new String[]{"Freshman", "Sophomore", "Junior", "Senior"};
    private final String[] tier_final_array = new String[]{"Android", "Java", "PHP", "AI", "React", "Node"};
    private EditText editText_date;
    Button button_submit, button_view_Attendance, button_view_Total_Attendance;
    Spinner branch_sp, year_sp, subject_sp;
    String major = "CS";
    String college_year = "Freshman";
    String course = "Android";
    private ImageButton button_date;
    private Calendar my_calendar;
    private int var_day, var_month, var_year;

}
