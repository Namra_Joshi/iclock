package com.android.iclock.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.iclock.bean.MarkAttendance;
import com.android.iclock.bean.Student;
import com.android.iclock.context.ContextiClock;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

public class RollCallPerStudent extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.markrollcall);

        list_View = (ListView) findViewById(R.id.listview);
        final ArrayList<String> attendance_ArrayList = new ArrayList<String>();
        attendance_ArrayList.add("Current Count Per Student");
        attendance_Bean_ArrayList = ((ContextiClock) RollCallPerStudent.this.getApplicationContext()).getRollCall();

        for (MarkAttendance attendance_Bean : attendance_Bean_ArrayList) {
            String role = "";

            MyDatabase adapter_database = new MyDatabase(RollCallPerStudent.this);
            Student student_Bean = adapter_database.retrieveStudent(attendance_Bean.retrieveStudentRollCall());
            role = attendance_Bean.retrieveStudentRollCall() + ".     " + student_Bean.retrieveFirstName() + "," + student_Bean.retrieveLastName() + "                  " + attendance_Bean.retrieveRollCallStatus();
            attendance_ArrayList.add(role);
        }

        Arraylist_Adapter = new ArrayAdapter<String>(this, R.layout.view_student_list, R.id.labelAttendancePerStudent, attendance_ArrayList);
        list_View.setAdapter(Arraylist_Adapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    private ListView list_View;
    private ArrayAdapter<String> Arraylist_Adapter;
    ArrayList<MarkAttendance> attendance_Bean_ArrayList;

}
