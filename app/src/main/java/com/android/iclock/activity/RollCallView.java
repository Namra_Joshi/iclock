package com.android.iclock.activity;

import com.android.iclock.R;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class RollCallView extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rollcallview);

        major_sp = (Spinner) findViewById(R.id.spinnerbranchView);
        college_year_sp = (Spinner) findViewById(R.id.spinneryearView);


        major_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.WHITE);
                major = (String) major_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> argument0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> major_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, major_string_array);
        major_adapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        major_sp.setAdapter(major_adapter);


        college_year_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.WHITE);
                college_year = (String) college_year_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> argument0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> college_year_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, college_year_array);
        college_year_adapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        college_year_sp.setAdapter(college_year_adapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }


    private String[] major_string_array = new String[]{"CS"};
    Spinner major_sp, college_year_sp;
    String major, college_year;
    private String[] college_year_array = new String[]{"Freshman", "Sophomore", "Junior", "Senior"};

}
