package com.android.iclock.activity;

import com.android.iclock.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class StudentView extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rollcallview);

        branch_sp = (Spinner) findViewById(R.id.spinnerbranchView);
        year_sp = (Spinner) findViewById(R.id.spinneryearView);


        branch_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.BLACK);
                major = (String) branch_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> argument0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> major_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, major_string_array);
        major_adapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        branch_sp.setAdapter(major_adapter);


        year_sp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> argument0, View view,
                                       int argument1, long argument2) {
                // TODO Auto-generated method stub
                ((TextView) argument0.getChildAt(0)).setTextColor(Color.BLACK);
                college_year = (String) year_sp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> argument0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> college_year_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, college_year_array);
        college_year_adapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year_sp.setAdapter(college_year_adapter);

        button_submit = (Button) findViewById(R.id.submitButton);
        button_submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View argument0) {

                Intent new_intent = new Intent(StudentView.this, StudentViewByMajor.class);
                new_intent.putExtra("branch", major);
                new_intent.putExtra("year", college_year);
                startActivity(new_intent);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    private String[] major_string_array = new String[]{"CS"};
    Spinner branch_sp, year_sp;
    String major, college_year;
    private String[] college_year_array = new String[]{"Freshman", "Sophomore", "Junior", "Senior"};
    Button button_submit;


}
