package com.android.iclock.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.iclock.bean.Student;
import com.android.iclock.db.MyDatabase;
import com.android.iclock.R;

public class StudentViewByMajor extends Activity {

    MyDatabase adapter_database = new MyDatabase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.markrollcall);

        list_View = (ListView) findViewById(R.id.listview);
        final ArrayList<String> new_student_ArrayList = new ArrayList<String>();

        major = getIntent().getExtras().getString("branch");
        college_year = getIntent().getExtras().getString("year");

        student_Bean_ArrayList = adapter_database.retrieveStudentsByMajor(major, college_year);

        for (Student student_Bean : student_Bean_ArrayList) {
            String role = student_Bean.retrieveFirstName() + "," + student_Bean.retrieveLastName();

            new_student_ArrayList.add(role);
            Log.d("users: ", role);

        }

        new_Arraylist_Adapter = new ArrayAdapter<String>(this, R.layout.access_student_list, R.id.label, new_student_ArrayList);
        list_View.setAdapter(new_Arraylist_Adapter);

        list_View.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> argument0, View argument1,
                                           final int position, long argument2) {
                AlertDialog.Builder new_alert_Dialog_Builder = new AlertDialog.Builder(StudentViewByMajor.this);
                new_alert_Dialog_Builder.setTitle(getTitle() + "decision");
                new_alert_Dialog_Builder.setMessage("Are you sure?");
                new_alert_Dialog_Builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        new_student_ArrayList.remove(position);
                        new_Arraylist_Adapter.notifyDataSetChanged();
                        new_Arraylist_Adapter.notifyDataSetInvalidated();

                        adapter_database.removeStudent(student_Bean_ArrayList.get(position).retrieveStudent());
                        student_Bean_ArrayList = adapter_database.retrieveStudentsByMajor(major, college_year);

                        for (Student student_Bean : student_Bean_ArrayList) {
                            String role = " First Name: " + student_Bean.retrieveFirstName() + "\n Last name:" + student_Bean.retrieveLastName();
                            new_student_ArrayList.add(role);
                            Log.d("users: ", role);

                        }
                    }

                });
                new_alert_Dialog_Builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        Toast.makeText(getApplicationContext(), "You chose cancel",
                                Toast.LENGTH_LONG).show();
                    }
                });
                AlertDialog new_alert_Dialog = new_alert_Dialog_Builder.create();
                new_alert_Dialog.show();
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    String major, college_year;
    private ListView list_View;
    private ArrayAdapter<String> new_Arraylist_Adapter;
    ArrayList<Student> student_Bean_ArrayList;


}
