package com.android.iclock.bean;

public class Faculty {


    public String retrieveLastName() {
        return stringLastName;
    }

    public String retrievePassword() {
        return stringPassword;
    }

    public void setPassword(String thePassword) {
        this.stringPassword = thePassword;
    }

    public void setLastName(String theLastName) {
        this.stringLastName = theLastName;
    }

    public String retrieveNumber() {
        return stringMobileNumber;
    }

    public void setNumber(String theMobileNumber) {
        this.stringMobileNumber = theMobileNumber;
    }

    public String retrieveAddress() {
        return stringAddress;
    }

    public void setAddress(String theAddress) {
        this.stringAddress = theAddress;
    }

    public String retrieveUserName() {
        return stringUserName;
    }

    public void setUserName(String theUsername) {
        this.stringUserName = theUsername;
    }

    public int retrieveInstructor() {
        return new_faculty_id;
    }

    public void setInstructor(int faculty_id) {
        this.new_faculty_id = faculty_id;
    }

    public String retrieveFirstName() {
        return stringFirstName;
    }

    public void setFirstName(String theFirstName) {
        this.stringFirstName = theFirstName;
    }

    private String stringMobileNumber, stringLastName, stringFirstName, stringPassword, stringUserName, stringAddress;
    private int new_faculty_id;
}
