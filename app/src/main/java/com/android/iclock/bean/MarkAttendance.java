package com.android.iclock.bean;

public class MarkAttendance {


    public String retrieveRollCall() {
        return status_string;
    }

    public void setRollCall(String theStatus) {
        this.status_string = theStatus;
    }

    public int retrieveStudentRollCall() {
        return new_student_id;
    }

    public void setStudentPresence(int attendance_student_id) {
        this.new_student_id = attendance_student_id;
    }

    public int retrieveRollCallStatus() {
        return new_session_id;
    }

    public void setRollCallStatus(int attendance_session_id) {
        this.new_session_id = attendance_session_id;
    }

    private String status_string;
    private int new_session_id, new_student_id;
}
