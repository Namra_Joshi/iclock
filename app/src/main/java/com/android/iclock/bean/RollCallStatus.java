package com.android.iclock.bean;

public class RollCallStatus {


    public String retrieveMajor() {
        return new_major_session;
    }

    public void setMajor(
            String attendance_session_department) {
        this.new_major_session = attendance_session_department;
    }

    public int retrieveRollCallStatus() {
        return new_session_id;
    }

    public void setRollCallStatus(int theSessionID) {
        this.new_session_id = theSessionID;
    }

    public int retrieveInstructorStatus() {
        return new_faculty_session_id;
    }

    public void setInstructorStatus(int theFacultySession) {
        this.new_faculty_session_id = theFacultySession;
    }

    public String retrieveCourse() {
        return new_subject_session;
    }

    public void setCourse(String theSubjectSession) {
        this.new_subject_session = theSubjectSession;
    }

    public String retrieveClass() {
        return new_course_session;
    }

    public void setClass(String theClassSession) {
        this.new_course_session = theClassSession;
    }

    public String retrieveDate() {
        return new_date_session;
    }

    public void setDate(String theDateSession) {
        this.new_date_session = theDateSession;
    }

    private String new_major_session, new_subject_session, new_date_session, new_course_session;
    private int new_session_id, new_faculty_session_id;

}
