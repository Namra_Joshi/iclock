package com.android.iclock.bean;

public class Student {


    public String retrieveNumber() {
        return stringMobileNumber;
    }

    public void setNumber(String theMobileNumber) {
        this.stringMobileNumber = theMobileNumber;
    }

    public int retrieveStudent() {
        return new_student_id;
    }

    public void setStudent(int student_id) {
        this.new_student_id = student_id;
    }

    public String retrieveFirstName() {
        return stringFirstName;
    }

    public void setFirstName(String theFirstName) {
        this.stringFirstName = theFirstName;
    }

    public String retrieveMajor() {
        return stringMajor;
    }

    public void setMajor(String theMajor) {
        this.stringMajor = theMajor;
    }

    public String retrieveLastName() {
        return stringLastName;
    }

    public void setLastName(String theLastName) {
        this.stringLastName = theLastName;
    }

    public String retrieveCourse() {
        return stringCourse;
    }

    public void setCourse(String theCourse) {
        this.stringCourse = theCourse;
    }

    public String retrieveAddress() {
        return stringAddress;
    }

    public void setAddress(String theAddress) {
        this.stringAddress = theAddress;
    }


    private String stringFirstName, stringCourse, stringMajor, stringAddress, stringMobileNumber, stringLastName;
    private int new_student_id;
}
