package com.android.iclock.context;

import java.util.ArrayList;

import android.app.Application;

import com.android.iclock.bean.MarkAttendance;
import com.android.iclock.bean.Faculty;
import com.android.iclock.bean.Student;

public class ContextiClock extends Application {

    public void setInstructor(Faculty theFacultyBean) {
        this.new_faculty_Bean = theFacultyBean;
    }

    public Faculty getInstructor() {
        return new_faculty_Bean;
    }

    public void setAttendanceBeanList(ArrayList<MarkAttendance> theAttendanceArrayList) {
        this.new_attendance_ArrayList = theAttendanceArrayList;
    }

    public ArrayList<Student> getStudent() {
        return new_student_ArrayList;
    }

    public ArrayList<MarkAttendance> getRollCall() {
        return new_attendance_ArrayList;
    }

    public void setStudent(ArrayList<Student> theStudentArrayList) {
        this.new_student_ArrayList = theStudentArrayList;
    }

    private Faculty new_faculty_Bean;
    private ArrayList<Student> new_student_ArrayList;
    private ArrayList<MarkAttendance> new_attendance_ArrayList;
}
