package com.android.iclock.db;

import java.util.ArrayList;

import com.android.iclock.bean.MarkAttendance;
import com.android.iclock.bean.RollCallStatus;
import com.android.iclock.bean.Faculty;
import com.android.iclock.bean.Student;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDatabase extends SQLiteOpenHelper {


    public MyDatabase(Context context) {
        super(context, DB, null, DB_VERSION_NUMBER);
    }


    public ArrayList<MarkAttendance> getAllAttendanceByStudent() {

        ArrayList<MarkAttendance> new_Arraylist = new ArrayList<MarkAttendance>();
        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT student_number,count(*) FROM roll_call where new_status='P' group by student_number";
        Log.d("new_query", new_query);
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            do {
                Log.d("student Id", "student Id:" + new_cursor.getString(0) + ", Count:" + new_cursor.getString(1));
                MarkAttendance attendance_Bean = new MarkAttendance();
                attendance_Bean.setStudentPresence(Integer.parseInt(new_cursor.getString(0)));
                attendance_Bean.setRollCallStatus(Integer.parseInt(new_cursor.getString(1)));
                new_Arraylist.add(attendance_Bean);

            } while (new_cursor.moveToNext());
        }
        return new_Arraylist;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        String new_query_Faculty = "CREATE TABLE " + FACULTY_INFORMATION + " (" +
                STRING_FACULTY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                STRING_FACULTY_FIRSTNAME + " TEXT, " +
                STRING_FACULTY_LASTNAME + " TEXT, " +
                STRING_FACULTY_NUMBER + " TEXT, " +
                STRING_FACULTY_ADDRESS + " TEXT," +
                STRING_FACULTY_USERNAME + " TEXT," +
                STRING_FACULTY_PASSWORD + " TEXT " + ")";
        Log.d("newInstructorQuery", new_query_Faculty);


        String new_query_Student = "CREATE TABLE " + STUDENT_INFORMATION + " (" +
                STRING_STUDENT + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                STRING_STUDENT_FIRSTNAME + " TEXT, " +
                STRING_STUDENT_LASTNAME + " TEXT, " +
                STRING_STUDENT_NUMBER + " TEXT, " +
                STRING_STUDENT_ADDRESS + " TEXT," +
                STRING_STUDENT_DEPARTMENT + " TEXT," +
                STRING_STUDENT_CLASS + " TEXT " + ")";
        Log.d("newStudentQuery", new_query_Student);


        String new_query_Session = "CREATE TABLE " + NEW_SESSION + " (" +
                STRING_SESSION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                STRING_FACULTY_SESSION + " INTEGER, " +
                STRING_DEPARTMENT_SESSION + " TEXT, " +
                STRING_CLASS_SESSION + " TEXT, " +
                STRING_DATE_SESSION + " DATE," +
                STRING_SUBJECT_SESSION + " TEXT" + ")";
        Log.d("newRollCall", new_query_Session);


        String new_query_Attendance = "CREATE TABLE " + NEW_ATTENDANCE + " (" +
                SESSION_ID + " INTEGER, " +
                STRING_STUDENT_ID + " INTEGER, " +
                STRING_STATUS + " TEXT " + ")";
        Log.d("newRollCallQuery", new_query_Attendance);
        try {
            database.execSQL(new_query_Faculty);
            database.execSQL(new_query_Student);
            database.execSQL(new_query_Session);
            database.execSQL(new_query_Attendance);
        } catch (Exception theException) {
            theException.printStackTrace();
            Log.e("Exception", theException.getMessage());
        }

    }

    public void insertInstructor(Faculty new_faculty_Bean) {
        SQLiteDatabase database = this.getWritableDatabase();

        String new_query = "INSERT INTO instructor (firstname_instructor,lastname_instructor,number_instructor,address_instructor,username_instructor,password_instructor) values ('" +
                new_faculty_Bean.retrieveFirstName() + "', '" +
                new_faculty_Bean.retrieveLastName() + "', '" +
                new_faculty_Bean.retrieveNumber() + "', '" +
                new_faculty_Bean.retrieveAddress() + "', '" +
                new_faculty_Bean.retrieveUserName() + "', '" +
                new_faculty_Bean.retrievePassword() + "')";
        Log.d("new_query", new_query);
        database.execSQL(new_query);
        database.close();
    }

    public Faculty instructorValidation(String theUserName, String thePassword) {

        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT * FROM instructor where username_instructor='" + theUserName + "' and password_instructor='" + thePassword + "'";
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            Faculty new_faculty_Bean = new Faculty();
            new_faculty_Bean.setInstructor(Integer.parseInt(new_cursor.getString(0)));
            new_faculty_Bean.setFirstName(new_cursor.getString(1));
            new_faculty_Bean.setLastName(new_cursor.getString(2));
            new_faculty_Bean.setNumber(new_cursor.getString(3));
            new_faculty_Bean.setAddress(new_cursor.getString(4));
            new_faculty_Bean.setUserName(new_cursor.getString(5));
            new_faculty_Bean.setPassword(new_cursor.getString(6));
            return new_faculty_Bean;
        }
        return null;
    }

    public ArrayList<Faculty> retrieveInstructors() {

        Log.d("retrieve", "retrieve");
        ArrayList<Faculty> new_Arraylist = new ArrayList<Faculty>();
        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT * FROM instructor";
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            do {
                Faculty new_faculty_Bean = new Faculty();
                new_faculty_Bean.setInstructor(Integer.parseInt(new_cursor.getString(0)));
                new_faculty_Bean.setFirstName(new_cursor.getString(1));
                new_faculty_Bean.setLastName(new_cursor.getString(2));
                new_faculty_Bean.setNumber(new_cursor.getString(3));
                new_faculty_Bean.setAddress(new_cursor.getString(4));
                new_faculty_Bean.setUserName(new_cursor.getString(5));
                new_faculty_Bean.setPassword(new_cursor.getString(6));
                new_Arraylist.add(new_faculty_Bean);
            } while (new_cursor.moveToNext());
        }
        return new_Arraylist;
    }

    public void removeInstructor(int theFaculty) {

        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "DELETE FROM instructor WHERE instructor_number=" + theFaculty;
        Log.d("new_query", new_query);
        database.execSQL(new_query);
        database.close();
    }


    public void insertStudent(Student new_Student_Bean) {

        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "INSERT INTO student (firstname_student,lastname_student,number_student,address_student,major_student,student_course) values ('" +
                new_Student_Bean.retrieveFirstName() + "', '" +
                new_Student_Bean.retrieveLastName() + "','" +
                new_Student_Bean.retrieveNumber() + "', '" +
                new_Student_Bean.retrieveAddress() + "', '" +
                new_Student_Bean.retrieveMajor() + "', '" +
                new_Student_Bean.retrieveCourse() + "')";
        Log.d("new_query", new_query);
        database.execSQL(new_query);
        database.close();
    }

    public ArrayList<Student> retrieveStudentsByMajor(String theMajor, String the_College_Year) {

        ArrayList<Student> new_Arraylist = new ArrayList<Student>();
        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT * FROM student where major_student='" + theMajor + "' and student_course='" + the_College_Year + "'";
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            do {
                Student new_Student_Bean = new Student();
                new_Student_Bean.setStudent(Integer.parseInt(new_cursor.getString(0)));
                new_Student_Bean.setFirstName(new_cursor.getString(1));
                new_Student_Bean.setLastName(new_cursor.getString(2));
                new_Student_Bean.setNumber(new_cursor.getString(3));
                new_Student_Bean.setAddress(new_cursor.getString(4));
                new_Student_Bean.setMajor(new_cursor.getString(5));
                new_Student_Bean.setCourse(new_cursor.getString(6));
                new_Arraylist.add(new_Student_Bean);
            } while (new_cursor.moveToNext());
        }
        return new_Arraylist;
    }

    public Student retrieveStudent(int theStudent) {
        Student new_Student_Bean = new Student();
        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT * FROM student where new_student_number=" + theStudent;
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            do {
                new_Student_Bean.setStudent(Integer.parseInt(new_cursor.getString(0)));
                new_Student_Bean.setFirstName(new_cursor.getString(1));
                new_Student_Bean.setLastName(new_cursor.getString(2));
                new_Student_Bean.setNumber(new_cursor.getString(3));
                new_Student_Bean.setAddress(new_cursor.getString(4));
                new_Student_Bean.setMajor(new_cursor.getString(5));
                new_Student_Bean.setCourse(new_cursor.getString(6));

            } while (new_cursor.moveToNext());
        }
        return new_Student_Bean;
    }

    public void removeStudent(int theStudent) {

        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "DELETE FROM student WHERE new_student_number=" + theStudent;
        Log.d("new_query", new_query);
        database.execSQL(new_query);
        database.close();
    }


    public int markPresence(RollCallStatus new_attendance_Session) {

        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "INSERT INTO roll_call_status (roll_call_faculty_number,major,course,date,subject) values ('" +
                new_attendance_Session.retrieveInstructorStatus() + "', '" +
                new_attendance_Session.retrieveMajor() + "','" +
                new_attendance_Session.retrieveClass() + "', '" +
                new_attendance_Session.retrieveDate() + "', '" +
                new_attendance_Session.retrieveCourse() + "')";
        Log.d("new_query", new_query);
        database.execSQL(new_query);
        String string_query = "select max(roll_call_number) from roll_call_status";
        Cursor new_cursor = database.rawQuery(string_query, null);
        if (new_cursor.moveToFirst()) {
            int new_SessionId = Integer.parseInt(new_cursor.getString(0));
            return new_SessionId;
        }
        database.close();
        return 0;
    }

    public ArrayList<MarkAttendance> retrieveTotalPresentStudent(RollCallStatus attendanceSessionBean) {

        int new_session_id = 0;
        ArrayList<MarkAttendance> new_Arraylist = new ArrayList<MarkAttendance>();
        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT * FROM roll_call_status where roll_call_faculty_number=" + attendanceSessionBean.retrieveInstructorStatus() + ""
                + " AND major='" + attendanceSessionBean.retrieveMajor() + "' AND course='" + attendanceSessionBean.retrieveClass() + "'" +
                " AND subject='" + attendanceSessionBean.retrieveCourse() + "'";
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            do {
                new_session_id = (Integer.parseInt(new_cursor.getString(0)));
                String string_query = "SELECT * FROM roll_call where roll_call_number=" + new_session_id + " order by student_number";
                Cursor cursor_new = database.rawQuery(string_query, null);
                if (cursor_new.moveToFirst()) {
                    do {
                        MarkAttendance new_Attendance_Bean = new MarkAttendance();
                        new_Attendance_Bean.setRollCallStatus(Integer.parseInt(cursor_new.getString(0)));
                        new_Attendance_Bean.setStudentPresence(Integer.parseInt(cursor_new.getString(1)));
                        new_Attendance_Bean.setRollCall(cursor_new.getString(2));
                        new_Arraylist.add(new_Attendance_Bean);

                    } while (cursor_new.moveToNext());
                }
                MarkAttendance attendance_Bean = new MarkAttendance();
                attendance_Bean.setRollCallStatus(0);
                attendance_Bean.setRollCall("Date : " + new_cursor.getString(4));
                new_Arraylist.add(attendance_Bean);

            } while (new_cursor.moveToNext());
        }

        return new_Arraylist;
    }

    public ArrayList<RollCallStatus> retrieveTotalPresentStudent() {

        ArrayList<RollCallStatus> new_Arraylist = new ArrayList<RollCallStatus>();
        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT * FROM roll_call_status";
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            do {
                RollCallStatus new_Attendance_Session = new RollCallStatus();
                new_Attendance_Session.setRollCallStatus(Integer.parseInt(new_cursor.getString(0)));
                new_Attendance_Session.setInstructorStatus(Integer.parseInt(new_cursor.getString(1)));
                new_Attendance_Session.setMajor(new_cursor.getString(2));
                new_Attendance_Session.setClass(new_cursor.getString(3));
                new_Attendance_Session.setDate(new_cursor.getString(4));
                new_Attendance_Session.setCourse(new_cursor.getString(5));
                new_Arraylist.add(new_Attendance_Session);

            } while (new_cursor.moveToNext());
        }
        return new_Arraylist;
    }

    public void insertRollCall(MarkAttendance new_Attendance_Bean) {

        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "INSERT INTO roll_call values (" +
                new_Attendance_Bean.retrieveRollCallStatus() + ", " +
                new_Attendance_Bean.retrieveStudentRollCall() + ", '" +
                new_Attendance_Bean.retrieveRollCall() + "')";
        Log.d("new_query", new_query);
        database.execSQL(new_query);
        database.close();
    }


    public ArrayList<MarkAttendance> retrieveRollCall(RollCallStatus attendanceSessionBean) {

        int new_session_id = 0;
        ArrayList<MarkAttendance> new_Arraylist = new ArrayList<MarkAttendance>();
        SQLiteDatabase database = this.getWritableDatabase();
        String new_query = "SELECT * FROM roll_call_status where roll_call_faculty_number=" + attendanceSessionBean.retrieveInstructorStatus() + ""
                + " AND major='" + attendanceSessionBean.retrieveMajor() + "' AND course='" + attendanceSessionBean.retrieveClass() + "'" +
                " AND date='" + attendanceSessionBean.retrieveDate() + "' AND subject='" + attendanceSessionBean.retrieveCourse() + "'";
        Cursor new_cursor = database.rawQuery(new_query, null);
        if (new_cursor.moveToFirst()) {
            do {
                new_session_id = (Integer.parseInt(new_cursor.getString(0)));
            } while (new_cursor.moveToNext());
        }
        String string_query = "SELECT * FROM roll_call where roll_call_number=" + new_session_id + " order by student_number";
        Cursor cursor_new = database.rawQuery(string_query, null);
        if (cursor_new.moveToFirst()) {
            do {
                MarkAttendance new_Attendance_Bean = new MarkAttendance();
                new_Attendance_Bean.setRollCallStatus(Integer.parseInt(cursor_new.getString(0)));
                new_Attendance_Bean.setStudentPresence(Integer.parseInt(cursor_new.getString(1)));
                new_Attendance_Bean.setRollCall(cursor_new.getString(2));
                new_Arraylist.add(new_Attendance_Bean);

            } while (cursor_new.moveToNext());
        }
        return new_Arraylist;
    }


    @Override
    public void onUpgrade(SQLiteDatabase database, int argument1, int argument2) {

        String new_query_Faculty = "CREATE TABLE " + FACULTY_INFORMATION + " (" +
                STRING_FACULTY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                STRING_FACULTY_FIRSTNAME + " TEXT, " +
                STRING_FACULTY_LASTNAME + " TEXT, " +
                STRING_FACULTY_NUMBER + " TEXT, " +
                STRING_FACULTY_ADDRESS + " TEXT," +
                STRING_FACULTY_USERNAME + " TEXT," +
                STRING_FACULTY_PASSWORD + " TEXT " + ")";
        Log.d("newInstructorQuery", new_query_Faculty);

        String new_query_Student = "CREATE TABLE " + STUDENT_INFORMATION + " (" +
                STRING_STUDENT + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                STRING_STUDENT_FIRSTNAME + " TEXT, " +
                STRING_STUDENT_LASTNAME + " TEXT, " +
                STRING_STUDENT_NUMBER + " TEXT, " +
                STRING_STUDENT_ADDRESS + " TEXT," +
                STRING_STUDENT_DEPARTMENT + " TEXT," +
                STRING_STUDENT_CLASS + " TEXT " + ")";
        Log.d("newStudentQuery", new_query_Student);

        String new_query_Session = "CREATE TABLE " + NEW_SESSION + " (" +
                STRING_SESSION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                STRING_FACULTY_SESSION + " INTEGER, " +
                STRING_DEPARTMENT_SESSION + " TEXT, " +
                STRING_CLASS_SESSION + " TEXT, " +
                STRING_DATE_SESSION + " TEXT," +
                STRING_SUBJECT_SESSION + " TEXT" + ")";
        Log.d("newRollCall", new_query_Session);


        String new_query_Attendance = "CREATE TABLE " + NEW_ATTENDANCE + " (" +
                SESSION_ID + " INTEGER, " +
                STRING_STUDENT_ID + " INTEGER, " +
                STRING_STATUS + " TEXT " + ")";
        Log.d("newRollCallQuery", new_query_Attendance);

        try {
            database.execSQL(new_query_Faculty);
            database.execSQL(new_query_Student);
            database.execSQL(new_query_Session);
            database.execSQL(new_query_Attendance);
        } catch (Exception theException) {
            theException.printStackTrace();
            Log.e("Exception", theException.getMessage());
        }
    }


    private static final int DB_VERSION_NUMBER = 1;
    private static final String DB = "iClock";
    private static final String FACULTY_INFORMATION = "instructor";
    private static final String STUDENT_INFORMATION = "student";
    private static final String NEW_SESSION = "roll_call_status";
    private static final String NEW_ATTENDANCE = "roll_call";
    private static final String STRING_FACULTY = "instructor_number";
    private static final String STRING_STUDENT_CLASS = "student_course";
    private static final String STRING_SESSION_ID = "roll_call_number";
    private static final String STRING_FACULTY_SESSION = "roll_call_faculty_number";
    private static final String STRING_DEPARTMENT_SESSION = "major";
    private static final String STRING_CLASS_SESSION = "course";
    private static final String STRING_DATE_SESSION = "date";
    private static final String STRING_SUBJECT_SESSION = "subject";
    private static final String SESSION_ID = "roll_call_number";
    private static final String STRING_STUDENT_ID = "student_number";
    private static final String STRING_STATUS = "new_status";
    private static final String STRING_FACULTY_FIRSTNAME = "firstname_instructor";
    private static final String STRING_FACULTY_LASTNAME = "lastname_instructor";
    private static final String STRING_FACULTY_NUMBER = "number_instructor";
    private static final String STRING_FACULTY_ADDRESS = "address_instructor";
    private static final String STRING_FACULTY_USERNAME = "username_instructor";
    private static final String STRING_FACULTY_PASSWORD = "password_instructor";
    private static final String STRING_STUDENT = "new_student_number";
    private static final String STRING_STUDENT_FIRSTNAME = "firstname_student";
    private static final String STRING_STUDENT_LASTNAME = "lastname_student";
    private static final String STRING_STUDENT_NUMBER = "number_student";
    private static final String STRING_STUDENT_ADDRESS = "address_student";
    private static final String STRING_STUDENT_DEPARTMENT = "major_student";


}